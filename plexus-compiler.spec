Name:       plexus-compiler
Version:    2.8.2
Release:    5
Epoch:      0
Summary:    Components to manipulate compilers for Plexus
License:    MIT and ASL 2.0 and ASL 1.1
URL:        https://github.com/codehaus-plexus/plexus-compiler
Source0:    https://github.com/codehaus-plexus/%{name}/archive/%{name}-%{version}.tar.gz
Source1:    LICENSE.MIT
Source2:    http://www.apache.org/licenses/LICENSE-2.0.txt

BuildRequires:  mvn(org.eclipse.tycho:org.eclipse.jdt.core)
BuildRequires:  mvn(org.codehaus.plexus:plexus-utils)
BuildRequires:  mvn(org.codehaus.plexus:plexus-container-default)
BuildRequires:  mvn(org.codehaus.plexus:plexus-components:pom:)
BuildRequires:  mvn(org.codehaus.plexus:plexus-component-metadata)
BuildRequires:  maven-local

BuildArch:  noarch

Provides:       plexus-compiler-extras
Provides:       plexus-compiler-pom
Provides:       plexus-compiler-javadoc
Obsoletes:      plexus-compiler-extras
Obsoletes:      plexus-compiler-pom
Obsoletes:      plexus-compiler-javadoc

%description
Plexus Compiler is a Plexus component to use different compilers
through a uniform API.


%prep
%autosetup -n %{name}-%{name}-%{version} -p1

cp %{SOURCE1} LICENSE.MIT
cp %{SOURCE2} LICENSE

%pom_disable_module plexus-compiler-aspectj plexus-compilers
%pom_disable_module plexus-compiler-javac-errorprone plexus-compilers
%pom_disable_module plexus-compiler-test

%mvn_package ":*::sources:" __noinstall
%mvn_package ":plexus-compiler{,s}" pom
%mvn_package ":*{csharp,eclipse,jikes}*" extras


%pom_xpath_remove "pom:dependency[pom:artifactId[text()='plexus-compiler-test']]" plexus-compilers

%pom_remove_plugin :maven-site-plugin

%build
%mvn_build -f

%install
%mvn_install

%files -f .mfiles
%license LICENSE LICENSE.MIT
%{_javadocdir}/%{name}
%dir %{_javadir}/%{name}
%{_javadir}/%{name}/*
%{_datadir}/maven-metadata/*
%{_datadir}/maven-poms/%{name}/*

%changelog
* Mon Nov 14 2022 wulei <wulei80@h-partners.com> - 0:2.8.2-5
- Modifying the source code package

* Wed Dec 4 2019 huyan <hu.huyan@huawei.com> - 0:2.8.2-4
- Package Initialization
